package com.twuc.webApp;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DateController {
    /* 2.3.2 */
    @PostMapping("/api/datetimes")
    MyDate getDate(@RequestBody MyDate mydate) {
        return mydate;
    }
}

