package com.twuc.webApp;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/users")
public class UserIdController {
    @GetMapping("/{userId}")
    String getUserId(@PathVariable Integer userId) {
        return "the user id is " + userId;
    }

    @GetMapping("/int/{userId}")
    String getUserId_int(@PathVariable int userId) {
        return "the user id is " + userId;
    }

    @GetMapping("/int/v2/{userId1}/{userId2}")
    String getUserId_two_params(@PathVariable int userId1, @PathVariable int userId2) {
        return "the user id is " + userId1 + " and " + userId2;
    }

    @GetMapping("/req/{userId}")
    String getUserId_bind_reques_params(@PathVariable int userId, @RequestParam("userName") String userName) {
        return "the user id is " + userId + " and user name is s" + userName;
    }

    /* 2.2.2 */
    @GetMapping("/default")
    String getUserId_default_param(@RequestParam(defaultValue = "Eddy") String userName) {
        return "the user name is " + userName;
    }

    /* 2.2.3 */
    @GetMapping("/default/collection")
    ArrayList<String> getUserId_collection(@RequestParam ArrayList<String> myparam) {
        return myparam;
    }


}
