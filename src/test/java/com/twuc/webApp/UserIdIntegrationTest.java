package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class UserIdIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_bind_other_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("the user id is 2"));
    }

    @Test
    void should_return_200_when_bind_other_param_int() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/int/2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("the user id is 2"));
    }

    @Test
    void should_return_200_when_bind_2_params() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/int/v2/2/3"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    void should_return_200_when_bind_request_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/req/2?userName=xiaoming"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    /* 2.2.2 */
    @Test
    void should_return_200_when_use_default_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/default"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    /* 2.2.3 */
    @Test
    void should_return_200_when_use_collection() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/default/collection?myparam=myValue1&myparam=myValue2&myparam=myValue3"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

    /* 2.3.2 */
    @Test
    void should_return_200_when_use_time() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/req/2?userName=Eddy"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }


}
