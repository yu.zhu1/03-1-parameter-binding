package com.twuc.webApp;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class UserIdTest {

    @Test
    void should_return_bind_user() {
        UserIdController userId = new UserIdController();
        String result = userId.getUserId(2);
        assertEquals("the user id is 2", result);
    }
}
