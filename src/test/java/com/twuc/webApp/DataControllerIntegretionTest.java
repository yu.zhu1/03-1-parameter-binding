package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
public class DataControllerIntegretionTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_post_datetime() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content("{\"dateTime\":\"2019-10-01T10:00:00Z\"}"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("{\"dateTime\":\"2019-10-01\"}"));
    }

    @Test
    void should_return_200_when_post_datetime_unormal() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/api/datetimes")
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .content("{\"dateTime\":\"2019-10-01\"}"))

                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("{\"dateTime\":\"2019-10-01\"}"));
    }
}
