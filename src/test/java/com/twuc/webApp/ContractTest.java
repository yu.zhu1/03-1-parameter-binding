package com.twuc.webApp;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;

public class ContractTest {

    @Test
    void should_convert_object_to_json() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        contract contract = new contract("xiaoming");
        String json = objectMapper.writeValueAsString(contract);
        System.out.println(json);
    }

    @Test
    void should_convert_json_to_object() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        contract result = objectMapper.readValue("{\"name\":\"xiaoming\"}", contract.class);
        assertEquals("xiaoming", result.getName());
    }


}
